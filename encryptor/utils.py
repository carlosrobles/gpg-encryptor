"""Encryptor utils module

This module exports the helper functions used in processing incoming requests
and encrypting their payloads.
"""
from flask import Flask, current_app as app, redirect
import gnupg
import os


def import_gpg_key(gpg, gpg_pubkey):
    """The import_gpg_key helper function imports a given gpg pubkey"""
    try:
        import_result = gpg.import_keys(gpg_pubkey)
        app = Flask(__name__)
        with app.app_context():
            app.logger.debug('successfully imported public key:\n%s', gpg_pubkey)
    except Exception as gpg_import_exc:
        raise RuntimeError('Error importing public key: %s', gpg_import_exc)
    return gpg, import_result


def request_is_valid_file(request):
    """The request_is_valid_file helper function returns True if a file payload
    is provided and False if it does not.
    """
    if 'input_file' in request.files:
        app.logger.debug('file upload request received')
        file = request.files['input_file']
        if file.filename == '':
            app.logger.warn('No file selected for upload')
            return False
        if file:
            return True
    else:
        return False


def encrypt_file(request, gpg, gpg_recipient):
    """The encrypt_file helper function reads an input file payload and encrypts
    its contents.
    """
    file = request.files['input_file']
    file_content = file.read().decode('ASCII').strip()
    app.logger.debug('file successfully read')
    try:
        encrypted_data = gpg.encrypt(file_content, gpg_recipient, always_trust=True)
        app.logger.debug('successfully encrypted input file contents')
    except Exception as enc_exc:
        app.logger.warn('error encrypting input file contents. error: %s', enc_exc)
    encrypted_string = str(encrypted_data)
    encrypted_result = {'encrypted_result': encrypted_string}
    app.logger.debug('encrypted_result %s', encrypted_result)
    return encrypted_result


def encrypt_json(request, gpg, gpg_recipient):
    """The encrypt_json helper function reads an input json payload and encrypts
    the 'input_string' key value.
    """
    json_data = request.get_json(force=True)
    input_string = json_data['input_string']
    try:
        encrypted_data = gpg.encrypt(input_string, gpg_recipient, always_trust=True)
        app.logger.debug('successfully encrypted input string')
    except Exception as enc_exc:
        app.logger.warn('error encrypting input string. error: %s', enc_exc)
        return redirect(request.url)
    encrypted_string = str(encrypted_data)
    encrypted_result = {'encrypted_result': encrypted_string}
    app.logger.debug('encrypted_result %s', encrypted_result)
    return encrypted_result
