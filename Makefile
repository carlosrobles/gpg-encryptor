down:
	docker-compose down

build: down
	docker-compose build

up: build
	docker-compose up -d

test: up
	docker-compose run --rm gpg-encryptor pytest -v

.PHONY: down build up test
