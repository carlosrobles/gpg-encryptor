# gpg-encryptor

A Python Flask http server that accepts a json input string or an input file (with string contents) and provides an ASCII armored GPG encrypted plaintext block. An instance of this program is available at https://gpg-encryptor.robles.io and uses the public key contained in `env_vars`.
<br>


## What you'll Need
`docker`, `docker-compose`, and `make`. You will also need `jq` if you want to display the json output in a plaintext block with actual newlines instead of a json payload with encoded newlines (`\n`).

## Usage
Launch a local instance of gpg-encryptor:

`make up`

<br>
Perform a GET request and receive an informational message:

```
curl --url http://127.0.0.1:5000/
{"message":"Please use a POST method with a string payload to receive an encrypted string"}
```

<br>
Perform a POST request to encrypt the string `secretpassword` and use `jq` to make it easy to copy and paste the encrypted output:

```
curl  --url http://127.0.0.1:5000 --request POST --header "Content-Type: application/json"  --data '{"input_string": "secretpassword"}' | jq -r .encrypted_result

 % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                Dload  Upload   Total   Spent    Left  Speed
100   603  100   569  100    34  33388   1995 --:--:-- --:--:-- --:--:-- 35562

-----BEGIN PGP MESSAGE-----

hQEMA0jvdjfTGBseAQgArtKgGYt2XMM0m4tffSyFWrfuO58cTO0adeEtN6Yp+VMS
we7zw4DjR9FfRogWntijkhHIE2TQZF1ytchjMHRv5o4GnxLYVoJUFsMlL5cImDDs
aL+qfZtEBen6hcwkrDNG2z4qGNkWKsCcM67JsDJ1caxCwH/cReJ1FSkWYt9KvDKO
NNNR5lpr1EZvm4ZXwZpKrUbzsi25HXdVDu5zUygi9YL8xAm/SVrdvlJSZ6I/wSG8
r7H4eNMb5R1rFUrBsBcu23MQ4BlbkzOsnKK1SVN1tis0HlA5TQk093Of/taQFM2X
0SkPyj9VvsQkH23cTeniSv6fMd1VgmrtPcgX4oC7vdJJAdtR9+2MbMAnwu2ildmO
Znz2UYEe4XTSLKS3s0SRCSFexE1wMy4DES/uqnvTlNN1CiEzrGvun+V+4cXtVArq
O8NslP36I32qaw==
=4T4S
-----END PGP MESSAGE-----
```

<br>
Perform a POST request to encrypt the string contents of the file `/home/user/file.txt` and use `jq` to make it easy to copy and paste the encrypted output:

```
curl --url http://127.0.0.1:5000 --request POST --form 'input_file=@/home/user/file.txt' | jq -r .encrypted_result

 % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                Dload  Upload   Total   Spent    Left  Speed
100  3835  100  2033  100  1802   125k   110k --:--:-- --:--:-- --:--:--  132k

-----BEGIN PGP MESSAGE-----

hQEMA0jvdjfTGBseAQgAxGWI6Q9c3QE2DYcgW+KEq1l6rRkBSEuBV+MBJRQxpFxu
f6C+COu+gqnkC2gSs6I3zmb8PiKzrcrEGfdGLyydz7YimlPz9mXGKoKdhC2jtV78
vd4H+/w9VHylkyj0RXOvYQBoniY05LF56iQzJmawaelTAN9sUNtmej/as7vJBO2s
qodpz/yhICALx/PsWET6iEkrA/5AjXQ5V73Bidm9Ur+zlNVWoryIboklbJOiifUu
8dTikrYL5NE8B0iGxRwRq+QBMb5kXEpTuDtvzEkDH+c7MOjnwZfelG72noAPDCF+
x/UCMxMrsoNmK5dgsGj3tXaxInr9B8Dxy/Mj9Rd0ktLqAfUOnbN01UbETzkgIIrU
Og+F0syf+NPE9YGGJIArc6TzUh2qztCygf6ivmuNdwoDDpj2IboNPomuhLGnVugi
6wEASD5Z9w2v1PhyWit+v1CZLeR3Zro2aMtt0NYJiarQ1po/d/XqRPo9VO8cfEB+
S9TnFBw4vnf7YLMdAXj3TKJfX39m6MSKRI5nDgR1cQuKnhaSByav1PhufTe6lL4W
LU04Zz0Qs/ElI0w7E2W7ru0CnogbDZP4mp6VbatW5vlXoBE3V1gc77UohRVjs2jA
h+PdIbG1d3rP3eJ+KePtdPt02mb98r9i3J868BNPkQQHPTn3/5JWYod1W7wpqC04
18QgonK/XenylhTktzq+zEPr1pUmzGT0fCQCaGNmBKMEOcWI9VrpkvT3l9rqbhe8
RwpwVYcC0QAk+SjIpI3wAMAmW50Mu6xxn82xVTQp+1HP99g56w1ALWEvdPMwuDjg
rDz/JhMfm+uQVDgttaM2IrFKqu+JCPLFGfkYIr9YXeIUUIGNB5IIgOGVserhepRb
Oddg2vPo4YRDrap1FT30WhKQV5a5OYIjaU5wLBIcQsjXt107iA6EU9R2p8/OQLmA
LKH934WTl/LyqpOicOP22li4xv1rQ0ZIY5LbP+UA5K+DMdWNrUgWCnc0nR9fjSc7
KpaqQb2wuPxpU/hKdMGnSn2bW2bjJmegiZwrCcNEpppBhSOY2oempL4+4n3kiIm6
/O3nkBMToIkiAz4og2zP/WeDizBEyNZmzG1vv6t/Ey+CFv0Xl+fuQDJFAWjEd11J
RrTG41X6gSfyEBlhg7G4g8XYFPHeuW4d6oNTTYj84X/4LGoQNwTjKVKInn2XJTF2
peWLmUAWmIst0hkGrARSYdw9c8K2SWaZFjNyo8l/fiuHUW9SHaGCcoxOlu5zXcVU
u+Wq827++3XHlKGynRuWvwspJZ4cn3Vnmg9dvBJajDnfN+xzZwhtMHu6zzuHLV2J
UyH1nJj8Fd/olHNFgnnGw+dUdrlhvy4KQKCo8bOaFw7wBLa4CNMPc2cZ44A0c+pj
79dJAnMiG4XBBuRJm5EfGAWejOv9ldegNd2kGltKI4jrT65En/NEyaRYRUH9S2lh
5zkfPjiF+gb+UPMuugmfpT8azkuQf/SNEoGmH46cjLgsUW7rqjuwJI2EYQyIKihu
Mo9MJ2BydnOPTOO+PI6l6Per8IHu1U6UfpVOIwtZrKij9T2YNY3KcKsPBORt/HOm
CoGmaADNHBU8egs5QmWqP0ccSFVqdCvL0efo/ZHaZn3fi1um/jgBQpnRz5pcd91B
aoEYPnn3H923NGyUC/9Opqo9GnpGJm9JsmB1Nq/47CXm0/chlzrhm7FKGoruY5+f
rXIurbLsN6ddSwV9VGsITEBfatBMWiFwlsFTLH3OvVjukmEOIbsBA1wXDx9NtoUF
+7x8Pc+Xs5OaWpOCO9bnaLzMQDSlYHzleL0hPfUELLWqk7VSuPvbS/ieAsHuS7W3
YKx7gIuMMHHM8dEvvRGvMZ3O7Ks=
=Ngby
-----END PGP MESSAGE-----
```

<br>
Run the basic tests located in rest_test.py:

```
make test

============================ test session starts =============================
platform linux -- Python 3.7.1, pytest-4.0.0, py-1.7.0, pluggy-0.8.0 -- /usr/local/bin/python
cachedir: .pytest_cache
rootdir: /app, inifile: pytest.ini
plugins: mock-1.10.0, env-0.6.2
collected 3 items

rest_test.py::test_get PASSED                                          [ 33%]
rest_test.py::test_json_post PASSED                                    [ 66%]
rest_test.py::test_file_post PASSED                                    [100%]
```

<br>
Stop and destroy the container

`make down`
