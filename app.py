from flask import Flask, jsonify, request
from encryptor.encryptor import GPGManager
import gnupg
import logging
import os


# Create wgsi instance of Flask object
app = Flask(__name__)

# Set a maximum file upload size of 6KB
# A payload over 6KB in size will raise a RequestEntityTooLarge exception
app.config['MAX_CONTENT_LENGTH'] = 6 * 1024

# Declare our gpg related vars and gpg object
gpg_pubkey = os.environ['GPG_PUBKEY']
gpg_recipient = os.environ['GPG_RECIPIENT']
dir_path = os.path.dirname(os.path.realpath(__file__))  # get current working dir
gpg = gnupg.GPG(gnupghome=dir_path)
gpg_manager = GPGManager(gpg, gpg_pubkey, gpg_recipient)


# Return informational message for GET requests

# Accept requests on /gpg-encryptor to support
# GCE load balancer. This can be undone once
# an nginx load balancer with rewrite can be put in place.
@app.route('/', methods=['GET'])
@app.route('/gpg-encryptor', methods=['GET'])
def display_info_message():
    app.logger.debug('GET request received')
    return jsonify({'message': 'Please use a POST method with a string payload to receive an encrypted string'})


# Return encrypted string for POST requests.
# We can accommodate a json string or small file payload

# Accept requests on /gpg-encryptor to support
# GCE load balancer. This can be undone once
# an nginx load balancer with rewrite can be put in place.
@app.route('/', methods=['POST'])
@app.route('/gpg-encryptor', methods=['POST'])
def return_encrypted_string():
    encrypted_result = {}
    app.logger.debug('POST request received')
    encrypted_result = gpg_manager.encrypt(request)
    return jsonify(encrypted_result)


# If we are not running this natively in debug, assume we are using gunicorn as wsgi
# and implement appropriate log handling. This means tying the app log level
# to the gunicorn log level.
if __name__ != '__main__':
    # Get gunicorn error log object
    # Set app log handlers to be managed by gunicorn
    # Set app log level to be same as gunicorn log level
    # (gunicorn log level is passed to gunicorn at startup)
    gunicorn_logger = logging.getLogger('gunicorn.error')
    app.logger.handlers = gunicorn_logger.handlers
    app.logger.setLevel(gunicorn_logger.level)
else:
    app.run(debug=True, host='0.0.0.0')
