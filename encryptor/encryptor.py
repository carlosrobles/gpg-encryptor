"""Encryptor module

This module exports the GPGManager class responsible for encrypting
input data.
"""
from encryptor.utils import import_gpg_key, request_is_valid_file, encrypt_file, encrypt_json
from flask import current_app as app, redirect


class GPGManager:
    """The GPGManager is responsible for generating the encryption string from
    provided string input."""

    def __init__(self, gpg, gpg_pubkey, gpg_recipient):
        """The GPGManager constructor imports the provided gpg public key"""
        self.gpg_recipient = gpg_recipient
        self.gpg, self.import_result = import_gpg_key(gpg, gpg_pubkey)

    def encrypt(self, request):
        """The GPGManager encrypt function processes input file and json
        payloads."""
        if request_is_valid_file(request):
            encrypted_result = encrypt_file(request, self.gpg, self.gpg_recipient)
            return encrypted_result
        elif request.is_json:
            encrypted_result = encrypt_json(request, self.gpg, self.gpg_recipient)
            return encrypted_result
        else:
            app.logger.warn('No valid file upload or json payload detected')
            return redirect(request.url)
