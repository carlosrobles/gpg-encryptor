from python:3.7-alpine3.8

MAINTAINER carlos.robles@gmail.com

COPY Pipfile Pipfile.lock /app/

WORKDIR /app

RUN apk add --no-cache --virtual .fetch-deps gpgme && \
    pip install pipenv && \
    pipenv install --deploy --system

COPY pytest.ini rest_test.py app.py /app/
COPY encryptor/ /app/encryptor/

EXPOSE 5000

CMD ["gunicorn", "--bind=0.0.0.0:5000", "--workers=2", "--log-level=warn", "app:app"]
