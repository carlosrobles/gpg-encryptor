from flask import jsonify
from io import BytesIO
import app
import json
import pytest


def load_json(response):
    """Load json from a response"""
    return json.loads(response.data.decode('utf8'))


def post_json(client, url, dict):
    """Post json dictionary to specified URL"""
    return client.post(url,
                       data=json.dumps(dict),
                       content_type='application/json')

def post_file(client, url, data):
    """Post file to specified URL"""
    return client.post(url,
                       data=data,
                       content_type='multipart/form-data')

@pytest.fixture
def client():
    """Instantiate an instance of the flask built-in Werkzeug test client"""
    client = app.app.test_client()
    yield client


def test_get(client):
    """Confirm successful GET with expected output"""
    message = 'Please use a POST method with a string payload to receive an encrypted string'
    response = client.get('/')
    assert response.status_code == 200
    assert load_json(response) == dict(message=message)


def test_json_post(client):
    """Confirm successful POST with example json payload"""
    response = post_json(client,
                         '/',
                         dict(input_string='test_string'))
    assert response.status_code == 200


def test_file_post(client):
    """Confirm successful POST with example file payload"""
    data = dict(input_file=(BytesIO('dummy contents'.encode('utf-8')), "dummy_file"))
    response = post_file(client, '/', data)
    assert response.status_code == 200
